package works.lmz.jobs.periodic

import works.lmz.common.stereotypes.SingletonBean

/**
 *
 * author: Irina Benediktovich - http://plus.google.com/+IrinaBenediktovich
 * @deprecated
 */
@SingletonBean
class BrokenJob extends Counter implements PeriodicJob{

	static long waitTime = 1100

	protected void execute(){
		super.execute()
		Thread.sleep(waitTime)
		throw new NullPointerException('Ooops...')
	}

	@Override
	Runnable getRunnable() {
		return this.&execute;
	}

	@Override
	Long getInitialDelay() {
		return 1
	}

	@Override
	Long getPeriodicDelay() {
		return 1
	}

	@Override
	Boolean isEnabled() {
		return true
	}
}
