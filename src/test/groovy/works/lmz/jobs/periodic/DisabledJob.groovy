package works.lmz.jobs.periodic

import works.lmz.common.stereotypes.SingletonBean

/**
 *
 * author: Irina Benediktovich - http://plus.google.com/+IrinaBenediktovich
 * @deprecated
 */
@SingletonBean
class DisabledJob extends Counter implements PeriodicJob{

	@Override
	Runnable getRunnable() {
		return this.&execute;
	}

	@Override
	Long getInitialDelay() {
		return 1
	}

	@Override
	Long getPeriodicDelay() {
		return 1
	}

	@Override
	Boolean isEnabled() {
		return false
	}
}
