package works.lmz.jobs.periodic.depr

import works.lmz.jobs.periodic.AbstractJob
import works.lmz.jobs.periodic.ScheduledJob

/**
 *
 * author: Irina Benediktovich - http://plus.google.com/+IrinaBenediktovich
 */
class WrapperFactory {

	public static AbstractJob wrapJob(ScheduledJob job){
		return new AbstractJob() {
			@Override
			Long getInitialDelay() {
				return 1
			}

			@Override
			Boolean isEnabled() {
				return job?.isEnabled()
			}

			@Override
			Runnable getRunnable() {
				return job?.getInstance()?.runnable
			}
		}
	}
}
